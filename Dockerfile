from debian:buster-20190506-slim

LABEL maintainer="Anders Draagen <andersdra@gmail.com>"

ARG DEBIAN_FRONTEND=noninteractive

ARG C_USER=container
ARG C_UID=1000
ARG C_GUID=1000
ARG C_HOME="/home/${C_USER}"

COPY scripts/* /

RUN chmod +x /*.bash /*.sh \
    && /container_init.bash

ENV USER=${C_USER}
ENV SHELL=/bin/bash
ENV HOME=${C_HOME}

USER ${C_USER}
WORKDIR ${C_HOME}

CMD ["/entrypoint.sh"]
