#!/bin/bash

/container_user.bash
chown "$C_UID:$C_GUID" /entrypoint.sh

apt-get update
apt-get install --no-install-recommends --yes "$(sort < /install)"

/container_software.bash
/container_cleanup.bash
