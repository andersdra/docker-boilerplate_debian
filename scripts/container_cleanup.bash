#!/bin/bash

# home folder permissions
chmod --recursive 771 "$C_HOME"
chown --recursive --from=0:0 "$C_USER:$C_USER" "$C_HOME/"

# cleanup
apt-get purge --yes "$(sort < /remove)"
apt-get -qq clean autoclean
apt-get -qq autoremove --yes
rm -rf /usr/share/man/*
rm -rf /tmp/*
rm -rf /var/log/*
rm --recursive --force /var/lib/{apt,dpkg,cache,log}
rm /*.bash
rm /{install,remove}
